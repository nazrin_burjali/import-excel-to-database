package com.mdm.excel;

import com.mdm.excel.domain.Qk;
import com.mdm.excel.helper.ExcelHelper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.util.List;

@SpringBootTest
class ExcelApplicationTests {

	@Test
	void contextLoads() {
		Assertions.assertDoesNotThrow(() -> {
			FileInputStream inputStream = new FileInputStream("C:\\Users\\nurlan.dadashov\\Desktop\\QK.xlsx");
			List<Qk> qkList = ExcelHelper.excelToQks(inputStream);
		});
	}

}
