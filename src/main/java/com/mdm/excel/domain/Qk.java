package com.mdm.excel.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Qk {
    private Integer id;
    private String emitentHesabi;
    private LocalDate muqavileTarixi;
    private String qeydiyyatNomresi;
    private LocalDate sonTarix;
    private int serviceClassId;
    private LocalDateTime creationDate;
    private int isVat;

}
