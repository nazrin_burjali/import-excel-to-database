package com.mdm.excel.repository;

import com.mdm.excel.domain.Qk;

import java.util.List;

public interface QkRepository {
    List<Qk>findAll();
    void saveAll(List<Qk> Qks);
}
