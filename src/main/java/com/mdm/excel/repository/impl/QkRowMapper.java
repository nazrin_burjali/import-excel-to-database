package com.mdm.excel.repository.impl;

import com.mdm.excel.domain.Qk;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
public class QkRowMapper implements RowMapper<Qk> {
    @Override
    public Qk mapRow(ResultSet rs, int i) throws SQLException {
        Qk qk = new Qk();
        qk.setId(rs.getInt("ID"));
        qk.setMuqavileTarixi((rs.getTimestamp("Start").toLocalDateTime().toLocalDate()));
        qk.setSonTarix((rs.getTimestamp("Finish").toLocalDateTime().toLocalDate()));
        qk.setEmitentHesabi(rs.getString("AccountNumber"));
        qk.setQeydiyyatNomresi(rs.getString("InstrumentNumber"));
        qk.setServiceClassId(rs.getInt("ServiceClassID"));
        qk.setCreationDate(rs.getTimestamp("CreationDate").toLocalDateTime());
        qk.setIsVat(rs.getInt("IsVat"));
        return qk;
    }
}
