package com.mdm.excel.repository.impl;

import com.mdm.excel.domain.Qk;
import com.mdm.excel.repository.QkRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

@Repository
public class QkRepositoryImpl implements QkRepository {
    private JdbcTemplate jdbcTemplate;
    private QkRowMapper qkRowMapper;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public QkRepositoryImpl(JdbcTemplate jdbcTemplate, QkRowMapper qkRowMapper, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.qkRowMapper = qkRowMapper;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<Qk> findAll() {
        String sql = "select ID, \"Start\", \"Finish\", \"AccountNumber\", \"InstrumentNumber\", \"ServiceClassID\", \"CreationDate\", \"IsVat\" from \"ServiceContract\" ";
        List<Qk> qkList = jdbcTemplate.query(sql, qkRowMapper);
        return qkList;
    }

    @Override
    public void saveAll(List<Qk> qks) {

        String sql = "INSERT INTO \"ServiceContract\"(\"Start\", \"Finish\", \"AccountNumber\", \"InstrumentNumber\", \"ServiceClassID\",\"CreationDate\")\n" +
                "VALUES (:start, :finish, :accNum, :instNum, :classId,:createDate)";
        List<Integer> classes = Arrays.asList(8234, 8235);
        for (Qk qk : qks) {
            for (Integer classId : classes) {

                LocalDateTime finish = qk.getSonTarix() == null ? null : LocalDateTime.of(qk.getSonTarix(), LocalTime.of(0, 0));

                MapSqlParameterSource params = new MapSqlParameterSource("start", LocalDateTime.of(qk.getMuqavileTarixi(), LocalTime.of(0, 0)))
                        .addValue("finish", finish == null ? LocalDateTime.of(9999, 12, 31, 0, 0) : finish)
                        .addValue("accNum", qk.getEmitentHesabi())
                        .addValue("instNum", qk.getQeydiyyatNomresi())
                        .addValue("classId", classId)
                        .addValue("createDate", LocalDateTime.now());

                namedParameterJdbcTemplate.update(sql, params);
            }
        }
    }
}

