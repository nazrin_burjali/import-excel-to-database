package com.mdm.excel.service;

import com.mdm.excel.domain.Qk;
import com.mdm.excel.helper.ExcelHelper;
import com.mdm.excel.repository.QkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ExcelService {
    @Autowired
    private QkRepository qkRepository;

    public void save(MultipartFile file) {
        try {
            List<Qk> qkList = ExcelHelper.excelToQks(file.getInputStream());
            qkRepository.saveAll(qkList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    public List<Qk> getAllQk() {
        return qkRepository.findAll();
    }
}
