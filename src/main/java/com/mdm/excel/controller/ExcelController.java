package com.mdm.excel.controller;

import com.mdm.excel.domain.Qk;
import com.mdm.excel.helper.ExcelHelper;
import com.mdm.excel.service.ExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@RestController
@RequestMapping("/api/excel")
public class ExcelController {
    @Autowired
    ExcelService fileService;

    @PostMapping("/upload")
    public void uploadFile(@RequestParam("file") MultipartFile file) {
        if (ExcelHelper.hasExcelFormat(file)) {
            fileService.save(file);
            }
   }

    @GetMapping("/qks")
    public List<Qk> getAll(){
        return fileService.getAllQk();
    }

}
