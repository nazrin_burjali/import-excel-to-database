package com.mdm.excel.helper;

import com.mdm.excel.domain.Qk;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ExcelHelper {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    public static boolean hasExcelFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public static List<Qk> excelToQks(InputStream is) {
        List<Qk> qkList = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        try {
            Workbook workbook = new XSSFWorkbook(is);
            Sheet sheet = workbook.getSheetAt(0);
            List<String> headers = Arrays.asList("QK siyahısı (Səhm və İstiqraz)  -  05.02.2021 10:28:15", "№", "Kontraqent", "Emitent hesabı", "Müqavilənin tarixi", "Qeydiyyat nömrəsi", "Qiymətli kağızların növü", "QK tarixi", "Son tarixi");
            for (Row currentRow : sheet) {
                Map<Integer, String> properties = new HashMap<>();
                int index = 1;
                for (Cell cell : currentRow) {
                    if (cell.toString().isEmpty())
                        continue;
                    if (headers.contains(cell.toString()))
                        continue;
                    properties.put(index, cell.toString());
                    index++;
                }

                if (!properties.isEmpty()) {
                    if (properties.values().stream().noneMatch(v -> v.startsWith("EM"))) {
                        Qk qk = new Qk();
                        qk.setEmitentHesabi(null);
                        qk.setMuqavileTarixi(StringUtils.isEmpty(properties.get(3)) ? null : LocalDate.parse(properties.get(3), formatter));
                        qk.setQeydiyyatNomresi(properties.get(4));
                        qk.setSonTarix(StringUtils.isEmpty(properties.get(7)) ? null : LocalDate.parse(properties.get(7), formatter));
                        qkList.add(qk);
                    } else {
                        Qk qk = new Qk();
                        qk.setEmitentHesabi(properties.get(3));
                        qk.setMuqavileTarixi(StringUtils.isEmpty(properties.get(4)) ? null : LocalDate.parse(properties.get(4), formatter));
                        qk.setQeydiyyatNomresi(properties.get(5));
                        qk.setSonTarix(StringUtils.isEmpty(properties.get(8)) ? null : LocalDate.parse(properties.get(8), formatter));
                        qkList.add(qk);
                    }
                }
            }
            workbook.close();
            return qkList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }
}
